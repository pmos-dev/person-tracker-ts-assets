import { ECommonsAdamantineFieldType } from 'tscommons-es-models-field';

import { IInteraction } from '../interfaces/iinteraction';
import { IData } from '../interfaces/idata';
import { IField } from '../interfaces/ifield';

import { EDataMethod } from '../enums/edata-method';

import { getFieldByName } from './field';

export function getRelevantData<T extends IData>(
		datas: T[],
		field: IField
): T[] {
	return datas
			.filter((data: IData): boolean => data.field === field.id)
			.filter((data: IData): boolean => data.value !== undefined && data.value !== '');
}

export function getRelevantDataByName<T extends IData>(
		datas: T[],
		fields: IField[],
		name: string
): T[] {
	const field: IField|undefined = getFieldByName(fields, name);
	if (!field) return [];

	return getRelevantData<T>(datas, field);
}

export function getRelevantArrayPositionData<T extends IData>(
		datas: T[],
		field: IField
): T|undefined {
	const matches: T[] = getRelevantData<T>(datas, field)
			.sort((a: IData, b: IData): number => {
				if (a.timestamp.getTime() < b.timestamp.getTime()) return -1;
				if (a.timestamp.getTime() > b.timestamp.getTime()) return 1;
				return 0;
			});

	if (matches.length === 0) return undefined;

	switch (field.method) {
		case EDataMethod.FIRST:
			return matches.shift();
		case EDataMethod.LATEST:
		case EDataMethod.SEQUENCE:
			return matches.pop();
		case EDataMethod.EARLIEST:
			if (![ ECommonsAdamantineFieldType.DATE, ECommonsAdamantineFieldType.TIME, ECommonsAdamantineFieldType.DATETIME ].includes(field.type)) {
				// cannot meaningful use earliest for non-dates, so use first instead
				return matches.shift();
			}

			matches
					.sort((a: IData, b: IData): number => {
						const aDate: Date|undefined = a.value as Date|undefined;
						const bDate: Date|undefined = b.value as Date|undefined;
		
						if (!aDate && !bDate) return 0;
						if (aDate && !bDate) return 1;
						if (!aDate && bDate) return -1;
		
						if (aDate!.getTime() < bDate!.getTime()) return -1;
						if (aDate!.getTime() > bDate!.getTime()) return 1;
		
						return 0;
					});
			return matches.shift();
	}
}

export function getRelevantArrayPositionDataByName<T extends IData>(
		datas: T[],
		fields: IField[],
		name: string
): T|undefined {
	const field: IField|undefined = getFieldByName(fields, name);
	if (!field) return undefined;

	return getRelevantArrayPositionData<T>(datas, field);
}

export function getRelevantArrayPositionDatasByNames<T extends IData>(
		datas: T[],
		fields: IField[],
		names: string[]
): Map<string, T> {
	const map: Map<string, T> = new Map<string, T>();

	for (const name of names) {
		const data: T|undefined = getRelevantArrayPositionDataByName<T>(
				datas,
				fields,
				name
		);
		if (data) map.set(name, data);
	}

	return map;
}

function getDataIfExists(
		relevants: Map<string, IData>,
		key: string
): number|string|boolean|Date|undefined {
	if (!relevants.has(key)) return undefined;

	return relevants.get(key)!.value;
}

export type TDataSummaryFieldKeyMapping = {
		key: string;
		fieldName: string;
}

export function buildDataSummary<
		DataT extends IData,
		SummaryT extends { [ field: string ]: number|string|boolean|Date|undefined }
>(
		data: DataT[],
		fields: IField[],
		mappings: TDataSummaryFieldKeyMapping[]
): SummaryT {
	const relevants: Map<string, IData> = getRelevantArrayPositionDatasByNames(
			data,
			fields,
			mappings
					.map((mapping: TDataSummaryFieldKeyMapping): string => mapping.fieldName)
	);

	const summary: SummaryT = {} as SummaryT;	// eslint-disable-line @typescript-eslint/consistent-type-assertions
	for (const mapping of mappings) {
		const valueOrUndefined: number|string|boolean|Date|undefined = getDataIfExists(relevants, mapping.fieldName);
		(summary as { [ field: string ]: number|string|boolean|Date|undefined })[mapping.key] = valueOrUndefined;
	}

	return summary;
}

export type TSummaryAndIndex<
		SummaryT extends { [ field: string ]: number|string|boolean|Date|undefined }
> = {
		index: number;
		interaction: IInteraction;
		summary: SummaryT;
};

export function buildIndexedDataSummaries<
		DataT extends IData,
		SummaryT extends { [ field: string ]: number|string|boolean|Date|undefined }
>(
		interactions: IInteraction[],
		data: DataT[],
		fields: IField[],
		mappings: TDataSummaryFieldKeyMapping[],
		sortDateField: string
): TSummaryAndIndex<SummaryT>[] {
	type TWithoutIndex = Omit<TSummaryAndIndex<SummaryT>, 'index'>;

	return interactions
			.map((interaction: IInteraction): TWithoutIndex => ({
					interaction: interaction,
					summary: buildDataSummary(
							data,
							fields,
							mappings
					)
			}))
			.sort((a: TWithoutIndex, b: TWithoutIndex): number => {
				const aDate: Date|undefined = a.summary[sortDateField] as Date|undefined;
				const bDate: Date|undefined = b.summary[sortDateField] as Date|undefined;

				if (!aDate && !bDate) return 0;
				if (aDate && !bDate) return 1;
				if (!aDate && bDate) return -1;

				if (aDate!.getTime() < bDate!.getTime()) return -1;
				if (aDate!.getTime() > bDate!.getTime()) return 1;

				return 0;
			})
			.map((summary: TWithoutIndex, i: number): TSummaryAndIndex<SummaryT> => ({
					index: i,
					...summary
			}));
}
