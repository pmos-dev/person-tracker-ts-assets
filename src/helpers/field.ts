import { IField } from '../interfaces/ifield';

export function getFieldByName(fields: IField[], name: string): IField|undefined {
	return fields
			.find((field: IField): boolean => field.name === name);
}

export function getFieldsByNames(fields: IField[], names: string[]): IField[] {
	return fields
			.filter((field: IField): boolean => names.includes(field.name));
}
