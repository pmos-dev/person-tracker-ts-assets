import { ECommonsStepFlowStepType } from 'tscommons-es-models-step-flow';

import { IData } from '../interfaces/idata';
import { IField } from '../interfaces/ifield';

import { TStepFlowRenderedFlow, TStepFlowRenderedStep, TStepFlowRenderedTriage } from '../types/tstep-flow-renderered';

// NB, these aren't used to determine the current step within the interaction any more, as it is stored directly
// Instead, they are retained for other potential uses

export function walkRenderedTriageAndDataToNextStep(
		stepFlowRenderedTriage: TStepFlowRenderedTriage,
		fields: IField[],
		datas: IData[],
		step: TStepFlowRenderedStep
): TStepFlowRenderedStep|undefined {
	switch (step.type) {
		case ECommonsStepFlowStepType.COMPLETE:
		case ECommonsStepFlowStepType.ABORT:
			return undefined;
		default:
			if (step.flows.length === 0) throw new Error('Step without any outflows exists and is not COMPLETE/ABORT. This is not valid.');
			break;
	}

	if (step.field === null) {
		// data step without a field; 1 single outflow must be defined
		if (step.flows.length !== 1) throw new Error('A data step exists without a field, and != 1 outflows. This is not supported');
		if (step.flows[0].value !== null) throw new Error('A data step exists without a field, but its outflow has a value condition. This is not supported');
		
		const outstep: TStepFlowRenderedStep|undefined = stepFlowRenderedTriage.steps
				.find((s: TStepFlowRenderedStep): boolean => s.uid === step.flows[0].outstep);
		if (!outstep) throw new Error(`No such outflow step exists for ${step.flows[0].outstep}`);

		return outstep;
	}

	// At this point we must have some kind of DATA step, as ROOT would have been picked up in the previous "without field" bit.
	// We also know there is a minimum of 1 outflow route, as the switch default would have picked it up above.

	const field: IField|undefined = fields
			.find((f: IField): boolean => f.uid === step.field);
	if (!field) throw new Error('No such field by that uid. This should not be possible');

	const data: IData|undefined = datas
			.find((d: IData): boolean => d.field === field.id);
	if (!data) {
		// no value set for this field yet, so assume this is the current step
		return step;
	}

	const defaultFlow: TStepFlowRenderedFlow|undefined = step.flows
			.find((f: TStepFlowRenderedFlow): boolean => f.value === null);
	const conditionFlows: TStepFlowRenderedFlow[] = step.flows
			.filter((f: TStepFlowRenderedFlow): boolean => defaultFlow === undefined || f.value !== null);

	for (const flow of conditionFlows) {
		if (flow.value === data.value) {
			// select this flow and traverse it

			const outstep: TStepFlowRenderedStep|undefined = stepFlowRenderedTriage.steps
					.find((s: TStepFlowRenderedStep): boolean => s.uid === flow.outstep);
			if (!outstep) throw new Error(`No such outflow step exists for ${step.flows[0].outstep}`);

			return outstep;
		}
	}
	
	// if has data and doesn't match a condition and a default exists, go with that
	if (defaultFlow) {
		// select this flow and traverse it

		const outstep: TStepFlowRenderedStep|undefined = stepFlowRenderedTriage.steps
				.find((s: TStepFlowRenderedStep): boolean => s.uid === defaultFlow.outstep);
		if (!outstep) throw new Error(`No such outflow step exists for ${step.flows[0].outstep}`);

		return outstep;
	}

	// No flow matched. Assume this is the current step
	return undefined;
}

export function walkRenderedTriageAndDataToCurrentStep(
		stepFlowRenderedTriage: TStepFlowRenderedTriage,
		fields: IField[],
		datas: IData[]
): TStepFlowRenderedStep|undefined {
	const history: string[] = [];

	const root: TStepFlowRenderedStep|undefined = stepFlowRenderedTriage.steps
			.find((s: TStepFlowRenderedStep): boolean => s.uid === stepFlowRenderedTriage.root);
	if (!root) return undefined;

	if (root.flows.length === 0) return undefined;
	if (root.flows.length > 1) throw new Error('Multiple flows for a root step. This is not supported');

	let step: TStepFlowRenderedStep = root;
	// outerloop:
	while (true) {
		if (history.includes(step.uid)) {
			// infinite loop. Abort at this step
			return step;
		}
		history.push(step.uid);

		const next: TStepFlowRenderedStep|undefined = walkRenderedTriageAndDataToNextStep(
				stepFlowRenderedTriage,
				fields,
				datas,
				step
		);
		if (!next) {
			// no next step possible. Possibly COMPLETE/ABORT or no flow can be matched (which is intentional for not yet set data)
			return step;
		}

		step = next;

		// switch (step.type) {
		// 	case ECommonsStepFlowStepType.COMPLETE:
		// 	case ECommonsStepFlowStepType.ABORT:
		// 		return step;
		// 	default:
		// 		if (step.flows.length === 0) throw new Error('Step without any outflows exists and is not COMPLETE/ABORT. This is not valid.');
		// 		break;
		// }

		// if (step.field === null) {
		// 	// data step without a field; 1 single outflow must be defined
		// 	if (step.flows.length !== 1) throw new Error('A data step exists without a field, and != 1 outflows. This is not supported');
		// 	if (step.flows[0].value !== null) throw new Error('A data step exists without a field, but its outflow has a value condition. This is not supported');
			
		// 	const outstep: TStepFlowRenderedStep|undefined = stepFlowRenderedTriage.steps
		// 			.find((s: TStepFlowRenderedStep): boolean => s.uid === step.flows[0].outstep);
		// 	if (!outstep) throw new Error(`No such outflow step exists for ${step.flows[0].outstep}`);

		// 	step = outstep;
		// 	continue outerloop;
		// }

		// // At this point we must have some kind of DATA step, as ROOT would have been picked up in the previous "without field" bit.
		// // We also know there is a minimum of 1 outflow route, as the switch default would have picked it up above.

		// const field: IField|undefined = fields
		// 		.find((f: IField): boolean => f.uid === step.field);
		// if (!field) throw new Error('No such field by that uid. This should not be possible');

		// const data: IData|undefined = datas
		// 		.find((d: IData): boolean => d.field === field.id);
		// if (!data) {
		// 	// no value set for this field yet, so assume this is the current step
		// 	return step;
		// }

		// const defaultFlow: TStepFlowRenderedFlow|undefined = step.flows
		// 		.find((f: TStepFlowRenderedFlow): boolean => f.value === null);
		// const conditionFlows: TStepFlowRenderedFlow[] = step.flows
		// 		.filter((f: TStepFlowRenderedFlow): boolean => defaultFlow === undefined || f.value !== null);

		// for (const flow of conditionFlows) {
		// 	if (flow.value === data.value) {
		// 		// select this flow and traverse it

		// 		const outstep: TStepFlowRenderedStep|undefined = stepFlowRenderedTriage.steps
		// 				.find((s: TStepFlowRenderedStep): boolean => s.uid === flow.outstep);
		// 		if (!outstep) throw new Error(`No such outflow step exists for ${step.flows[0].outstep}`);

		// 		step = outstep;
		// 		continue outerloop;
		// 	}
		// }
		
		// // if has data and doesn't match a condition and a default exists, go with that
		// if (defaultFlow) {
		// 	// select this flow and traverse it

		// 	const outstep: TStepFlowRenderedStep|undefined = stepFlowRenderedTriage.steps
		// 			.find((s: TStepFlowRenderedStep): boolean => s.uid === defaultFlow.outstep);
		// 	if (!outstep) throw new Error(`No such outflow step exists for ${step.flows[0].outstep}`);

		// 	step = outstep;
		// 	continue outerloop;
		// }

		// // No flow matched. Assume this is the current step
		// return step;
	}
}

export function walkRenderedTriageAndDataToPreviousStep(
		stepFlowRenderedTriage: TStepFlowRenderedTriage,
		fields: IField[],
		datas: IData[],
		currentStep: TStepFlowRenderedStep
): TStepFlowRenderedStep|undefined {
	// this is smart as it only allows for 'last' to be a data step with a field, not others or un-fielded

	const history: string[] = [];
	let last: TStepFlowRenderedStep|undefined;

	const root: TStepFlowRenderedStep|undefined = stepFlowRenderedTriage.steps
			.find((s: TStepFlowRenderedStep): boolean => s.uid === stepFlowRenderedTriage.root);
	if (!root) return undefined;

	if (root.flows.length === 0) return undefined;
	if (root.flows.length > 1) throw new Error('Multiple flows for a root step. This is not supported');

	let step: TStepFlowRenderedStep = root;
	outerloop: while (true) {
		if (step.uid === currentStep.uid) return last;

		if (history.includes(step.uid)) {
			// infinite loop. Abort at this step (return last)
			return last;
		}
		history.push(step.uid);

		switch (step.type) {
			case ECommonsStepFlowStepType.COMPLETE:
			case ECommonsStepFlowStepType.ABORT:
				// walk completed without encountering the "current step". return whatever is currently last
				return last;
			default:
				if (step.flows.length === 0) throw new Error('Step without any outflows exists and is not COMPLETE/ABORT. This is not valid.');
				break;
		}

		if (step.field === null) {
			// data step without a field; 1 single outflow must be defined
			if (step.flows.length !== 1) throw new Error('A data step exists without a field, and != 1 outflows. This is not supported');
			if (step.flows[0].value !== null) throw new Error('A data step exists without a field, but its outflow has a value condition. This is not supported');
			
			const outstep: TStepFlowRenderedStep|undefined = stepFlowRenderedTriage.steps
					.find((s: TStepFlowRenderedStep): boolean => s.uid === step.flows[0].outstep);
			if (!outstep) throw new Error(`No such outflow step exists for ${step.flows[0].outstep}`);

			// don't set the last to this, as there is no field
			step = outstep;
			continue outerloop;
		}

		// At this point we must have some kind of DATA step, as ROOT would have been picked up in the previous "without field" bit.
		// We also know there is a minimum of 1 outflow route, as the switch default would have picked it up above.

		const field: IField|undefined = fields
				.find((f: IField): boolean => f.uid === step.field);
		if (!field) throw new Error('No such field by that uid. This should not be possible');

		const data: IData|undefined = datas
				.find((d: IData): boolean => d.field === field.id);
		if (!data) {
			// no value set for this field yet, we cannot walk forward, even though we haven't encountered the "current step"
			// return whatever is currently last
			return last;
		}

		const defaultFlow: TStepFlowRenderedFlow|undefined = step.flows
				.find((f: TStepFlowRenderedFlow): boolean => f.value === null);
		const conditionFlows: TStepFlowRenderedFlow[] = step.flows
				.filter((f: TStepFlowRenderedFlow): boolean => defaultFlow === undefined || f.value !== null);

		for (const flow of conditionFlows) {
			if (flow.value === data.value) {
				// select this flow and traverse it

				const outstep: TStepFlowRenderedStep|undefined = stepFlowRenderedTriage.steps
						.find((s: TStepFlowRenderedStep): boolean => s.uid === flow.outstep);
				if (!outstep) throw new Error(`No such outflow step exists for ${step.flows[0].outstep}`);

				// this route has a value, so replace the last step with it
				last = step;

				step = outstep;
				continue outerloop;
			}
		}
		
		// if has data and doesn't match a condition and a default exists, go with that
		if (defaultFlow) {
			// select this flow and traverse it

			const outstep: TStepFlowRenderedStep|undefined = stepFlowRenderedTriage.steps
					.find((s: TStepFlowRenderedStep): boolean => s.uid === defaultFlow.outstep);
			if (!outstep) throw new Error(`No such outflow step exists for ${step.flows[0].outstep}`);

			// this route doesn't have a value, but that's valid for this particular field/step, so replace the last step with it
			last = step;

			step = outstep;
			continue outerloop;
		}

		// No flow matched. Assume this is the actual current step
		// The walk didn't encounter the "current step", so have to return last
		return last;
	}
}
