import { ICommonsSecondClass, isICommonsSecondClass } from 'tscommons-es-models';
import { ICommonsUniquelyIdentifiedBase62, isICommonsUniquelyIdentifiedBase62 } from 'tscommons-es-models';

import { IEvent } from './ievent';

export interface IInteraction extends ICommonsSecondClass<IEvent>, ICommonsUniquelyIdentifiedBase62 {
		event: number;
}

export function isIInteraction(test: unknown): test is IInteraction {
	if (!isICommonsSecondClass<IEvent>(test, 'event')) return false;
	if (!isICommonsUniquelyIdentifiedBase62(test)) return false;
	
	return true;
}
