import { ICommonsStepFlowStep } from 'tscommons-es-models-step-flow';
import { ICommonsStepFlowFlow, isICommonsStepFlowFlow } from 'tscommons-es-models-step-flow';

import { IStep } from './istep';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IFlow extends ICommonsStepFlowFlow<ICommonsStepFlowStep> {}

export function isIFlow(test: unknown): test is IFlow {
	if (!isICommonsStepFlowFlow<IStep>(test)) return false;
	
	return true;
}
