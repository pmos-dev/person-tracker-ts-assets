import { commonsTypeHasPropertyString } from 'tscommons-es-core';
import { ICommonsSecondClass, ICommonsUniquelyIdentifiedBase62, isICommonsSecondClass, isICommonsUniquelyIdentifiedBase62 } from 'tscommons-es-models';
import { ICommonsManaged, isICommonsManaged } from 'tscommons-es-models-adamantine';

import { INamespace } from './inamespace';

export interface ITriage extends ICommonsManaged, ICommonsSecondClass<INamespace>, ICommonsUniquelyIdentifiedBase62 {
		namespace: number;
		
		description: string;
}

export function isITriage(test: unknown): test is ITriage {
	if (!isICommonsSecondClass<INamespace>(test, 'namespace')) return false;
	if (!isICommonsManaged(test)) return false;
	if (!isICommonsUniquelyIdentifiedBase62(test)) return false;
	
	if (!commonsTypeHasPropertyString(test, 'description')) return false;
	
	return true;
}
