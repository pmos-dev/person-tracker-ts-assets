import {
		commonsTypeHasPropertyDate,
		commonsTypeHasPropertyNumberOrUndefined,
		commonsTypeHasPropertyStringOrUndefined,
		commonsTypeHasPropertyBooleanOrUndefined,
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyDateOrUndefined
} from 'tscommons-es-core';
import { ICommonsSecondClass, ICommonsUniquelyIdentifiedBase62, isICommonsSecondClass, isICommonsUniquelyIdentifiedBase62 } from 'tscommons-es-models';

import { IEvent } from './ievent';

// Ideally IData should use Namespace as its parent firstclass, as data for persons could be cross-event.
// However it's complex trying to sort out "since logno" style sync when you don't know which event we are syncing from.
// So leave it as per-event for now.

export interface IData extends ICommonsSecondClass<IEvent>, ICommonsUniquelyIdentifiedBase62 {
		event: number;

		logno: number;
		timestamp: Date;

		field: number;
		
		value: number|string|boolean|Date|undefined;
}

export function isIData(test: unknown): test is IData {
	if (!isICommonsSecondClass<IEvent>(test, 'event')) return false;
	if (!isICommonsUniquelyIdentifiedBase62(test)) return false;
	
	if (!commonsTypeHasPropertyNumber(test, 'logno')) return false;
	if (!commonsTypeHasPropertyDate(test, 'timestamp')) return false;
	
	if (!commonsTypeHasPropertyNumber(test, 'field')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'person')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'interaction')) return false;

	if (test['person'] === undefined && test['interaction'] === undefined) return false;
	if (test['person'] !== undefined && test['interaction'] !== undefined) return false;

	if (
		!commonsTypeHasPropertyNumberOrUndefined(test, 'value')
		&& !commonsTypeHasPropertyStringOrUndefined(test, 'value')
		&& !commonsTypeHasPropertyBooleanOrUndefined(test, 'value')
		&& !commonsTypeHasPropertyDateOrUndefined(test, 'value')
	) return false;

	return true;
}

export type IInteractionData<DataT extends IData = IData> = DataT & {
		interaction: number;
}

export function isIInteractionData<DataT extends IData = IData>(test: unknown): test is IInteractionData<DataT> {
	if (!isIData(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, 'interaction')) return false;

	return true;
}

export type IPersonData<DataT extends IData = IData> = DataT & {
		person: number;
}

export function isIPersonData<DataT extends IData = IData>(test: unknown): test is IPersonData<DataT> {
	if (!isIData(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, 'person')) return false;

	return true;
}
