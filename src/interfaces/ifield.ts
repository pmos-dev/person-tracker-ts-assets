import { commonsTypeHasPropertyEnum } from 'tscommons-es-core';
import { ICommonsOrientatedOrdered, ICommonsUniquelyIdentifiedBase62, isICommonsOrientatedOrdered, isICommonsUniquelyIdentifiedBase62 } from 'tscommons-es-models';
import { ICommonsManaged, isICommonsManaged } from 'tscommons-es-models-adamantine';
import { ICommonsAdamantineField, isICommonsAdamantineField } from 'tscommons-es-models-field';

import { EFieldContext, isEFieldContext } from '../enums/efield-context';
import { EDataMethod, isEDataMethod } from '../enums/edata-method';

import { INamespace } from './inamespace';

export interface IField extends ICommonsManaged, ICommonsOrientatedOrdered<INamespace>, ICommonsAdamantineField, ICommonsUniquelyIdentifiedBase62 {
		namespace: number;
		
		context: EFieldContext;
		method: EDataMethod;
}

export function isIField(test: unknown): test is IField {
	if (!isICommonsOrientatedOrdered<INamespace>(test, 'namespace')) return false;
	if (!isICommonsManaged(test)) return false;
	if (!isICommonsAdamantineField(test)) return false;
	if (!isICommonsUniquelyIdentifiedBase62(test)) return false;
	
	if (!commonsTypeHasPropertyEnum<EFieldContext>(test, 'context', isEFieldContext)) return false;
	if (!commonsTypeHasPropertyEnum<EDataMethod>(test, 'method', isEDataMethod)) return false;
	
	return true;
}
