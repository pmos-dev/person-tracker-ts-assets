import { ICommonsOrdered, isICommonsOrdered } from 'tscommons-es-models';
import { ICommonsManaged, isICommonsManaged } from 'tscommons-es-models-adamantine';

export interface INamespace extends ICommonsManaged, ICommonsOrdered {}

export function isINamespace(test: unknown): test is INamespace {
	if (!isICommonsManaged(test)) return false;
	if (!isICommonsOrdered(test)) return false;
	
	return true;
}
