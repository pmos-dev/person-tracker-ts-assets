import { commonsTypeHasPropertyDate, commonsTypeHasPropertyNumber, commonsTypeHasPropertyNumberOrUndefined } from 'tscommons-es-core';
import { ICommonsSecondClass, isICommonsSecondClass } from 'tscommons-es-models';

import { IInteraction } from './iinteraction';

export interface IInteractionStep extends ICommonsSecondClass<IInteraction> {
		interaction: number;
		step: number;
		timestamp: Date;

		// This isn't be used at the moment. Future possibilities
		data?: number;
}

export function isIInteractionStep(test: unknown): test is IInteractionStep {
	if (!isICommonsSecondClass<IInteraction>(test, 'interaction')) return false;
	
	if (!commonsTypeHasPropertyNumber(test, 'step')) return false;
	if (!commonsTypeHasPropertyDate(test, 'timestamp')) return false;

	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'data')) return false;
	
	return true;
}
