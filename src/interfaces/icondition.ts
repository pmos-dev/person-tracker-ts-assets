import {
		commonsTypeHasProperty,
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyBoolean
} from 'tscommons-es-core';
import { ICommonsStepFlowCondition, isICommonsStepFlowCondition } from 'tscommons-es-models-step-flow';

import { IFlow } from './iflow';
import { IStep } from './istep';

export interface ICondition extends ICommonsStepFlowCondition<
		IFlow,
		IStep
> {
		value: number|string|boolean|undefined;
}

export function isICondition(test: unknown): test is ICondition {
	if (!isICommonsStepFlowCondition(test)) return false;

	if (!commonsTypeHasProperty(test, 'value')) return false;
	if (
		test['value'] !== undefined
		&& !commonsTypeHasPropertyNumber(test, 'value')
		&& !commonsTypeHasPropertyString(test, 'value')
		&& !commonsTypeHasPropertyBoolean(test, 'value')
	) return false;
	
	return true;
}
