import { ICommonsSecondClass, isICommonsSecondClass } from 'tscommons-es-models';
import { ICommonsUniquelyIdentifiedBase62, isICommonsUniquelyIdentifiedBase62 } from 'tscommons-es-models';

import { IEvent } from './ievent';

export interface IPerson extends ICommonsSecondClass<IEvent>, ICommonsUniquelyIdentifiedBase62 {
		event: number;
}

export function isIPerson(test: unknown): test is IPerson {
	if (!isICommonsSecondClass<IEvent>(test, 'event')) return false;
	if (!isICommonsUniquelyIdentifiedBase62(test)) return false;
	
	return true;
}
