import { commonsTypeHasPropertyNumberOrUndefined } from 'tscommons-es-core';
import { ICommonsSecondClass, isICommonsSecondClass } from 'tscommons-es-models';
import { ICommonsStepFlowStep, isICommonsStepFlowStep } from 'tscommons-es-models-step-flow';

import { ITriage } from './itriage';

export interface IStep extends ICommonsSecondClass<ITriage>, ICommonsStepFlowStep {
		triage: number;
		
		field?: number;
}

export function isIStep(test: unknown): test is IStep {
	if (!isICommonsStepFlowStep(test)) return false;

	if (!isICommonsSecondClass<ITriage>(test, 'triage')) return false;

	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'field')) return false;
	
	return true;
}
