import { commonsTypeHasPropertyDate, commonsTypeHasPropertyString } from 'tscommons-es-core';
import { ICommonsSecondClass, isICommonsSecondClass } from 'tscommons-es-models';
import { ICommonsUniquelyIdentifiedBase62, isICommonsUniquelyIdentifiedBase62 } from 'tscommons-es-models';
import { ICommonsManaged, isICommonsManaged } from 'tscommons-es-models-adamantine';

import { INamespace } from './inamespace';

export interface IEvent extends ICommonsManaged, ICommonsSecondClass<INamespace>, ICommonsUniquelyIdentifiedBase62 {
		namespace: number;
		
		start: Date;
		end: Date;
		description: string;
}

export function isIEvent(test: unknown): test is IEvent {
	if (!isICommonsSecondClass<INamespace>(test, 'namespace')) return false;
	if (!isICommonsManaged(test)) return false;
	if (!isICommonsUniquelyIdentifiedBase62(test)) return false;
	
	if (!commonsTypeHasPropertyDate(test, 'start')) return false;
	if (!commonsTypeHasPropertyDate(test, 'end')) return false;
	if (!commonsTypeHasPropertyString(test, 'description')) return false;
	
	return true;
}
