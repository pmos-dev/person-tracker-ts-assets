import {
		commonsTypeHasPropertyDate,
		commonsTypeHasPropertyNumberOrUndefined,
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyString
} from 'tscommons-es-core';
import { ICommonsSecondClass, ICommonsUniquelyIdentifiedBase62, isICommonsSecondClass, isICommonsUniquelyIdentifiedBase62 } from 'tscommons-es-models';

import { IEvent } from './ievent';

// Ideally INote should use Namespace as its parent firstclass, as notes for persons could be cross-event.
// However it's complex trying to sort out "since logno" style sync when you don't know which event we are syncing from.
// So leave it as per-event for now.

export interface INote extends ICommonsSecondClass<IEvent>, ICommonsUniquelyIdentifiedBase62 {
		event: number;

		logno: number;
		timestamp: Date;
		
		value: string;
}

export function isINote(test: unknown): test is INote {
	if (!isICommonsSecondClass<IEvent>(test, 'event')) return false;
	if (!isICommonsUniquelyIdentifiedBase62(test)) return false;
	
	if (!commonsTypeHasPropertyNumber(test, 'logno')) return false;
	if (!commonsTypeHasPropertyDate(test, 'timestamp')) return false;
	
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'person')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'interaction')) return false;
	
	if (test['person'] === undefined && test['interaction'] === undefined) return false;
	if (test['person'] !== undefined && test['interaction'] !== undefined) return false;
	
	if (!commonsTypeHasPropertyString(test, 'value')) return false;

	return true;
}

export type IInteractionNote<NoteT extends INote = INote> = NoteT & {
		interaction: number;
}

export function isIInteractionNote<NoteT extends INote = INote>(test: unknown): test is IInteractionNote<NoteT> {
	if (!isINote(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, 'interaction')) return false;

	return true;
}

export type IPersonNote<NoteT extends INote = INote> = NoteT & {
		person: number;
}

export function isIPersonNote<NoteT extends INote = INote>(test: unknown): test is IPersonNote<NoteT> {
	if (!isINote(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, 'person')) return false;

	return true;
}
