import { commonsTypeEncodePropertyObject, TEncodedObject } from 'tscommons-es-core';

import { IPerson, isIPerson } from '../interfaces/iperson';

export type TPerson = Readonly<IPerson> & TEncodedObject;

export function isTPerson(test: unknown): test is TPerson {
	if (!isIPerson(test)) return false;
	
	return true;
}

export function encodeIPerson(person: IPerson): TPerson {
	return commonsTypeEncodePropertyObject({ ...person }) as TPerson;
}

export function decodeIPerson(encoded: TPerson): IPerson {
	return { ...encoded };
}
