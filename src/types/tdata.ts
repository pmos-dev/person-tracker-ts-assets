import {
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyNumberOrUndefined,
		commonsTypeHasPropertyStringOrUndefined,
		commonsTypeHasPropertyBooleanOrUndefined,
		commonsDateDateToYmdHis,
		commonsDateYmdHisToDate,
		COMMONS_REGEX_PATTERN_DATETIME_YMDHIS,
		commonsTypeIsDate,
		commonsTypeIsString,
		TEncodedObject
} from 'tscommons-es-core';
import { ICommonsSecondClass, ICommonsUniquelyIdentifiedBase62, isICommonsUniquelyIdentifiedBase62 } from 'tscommons-es-models';

import { IData, IInteractionData, IPersonData, isIInteractionData, isIPersonData } from '../interfaces/idata';
import { TEvent } from './tevent';

export type TData = ICommonsSecondClass<TEvent> & ICommonsUniquelyIdentifiedBase62 & {
		event: number;
		logno: number;
		field: number;
		timestamp: string;
		value: string|number|boolean|null;
} & TEncodedObject;

export type TPersonData = TData & { person: number };
export type TInteractionData = TData & { interaction: number };

export function isTData(test: unknown): test is TData {
	if (!commonsTypeHasPropertyNumber(test, 'event')) return false;

	if (!isICommonsUniquelyIdentifiedBase62(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, 'id')) return false;

	if (!commonsTypeHasPropertyNumber(test, 'logno')) return false;

	if (!commonsTypeHasPropertyNumber(test, 'field')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'person')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'interaction')) return false;

	if (test['person'] === undefined && test['interaction'] === undefined) return false;
	if (test['person'] !== undefined && test['interaction'] !== undefined) return false;

	if (!commonsTypeHasPropertyString(test, 'timestamp')) return false;
	if (!COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(test['timestamp'] as string)) return false;

	if (
		!commonsTypeHasPropertyNumberOrUndefined(test, 'value')
		&& !commonsTypeHasPropertyStringOrUndefined(test, 'value')
		&& !commonsTypeHasPropertyBooleanOrUndefined(test, 'value')
		&& test['value'] !== null
	) return false;
	
	return true;
}

export function isTPersonData(test: unknown): test is TPersonData {
	if (!isTData(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, 'person')) return false;

	return true;
}

export function isTInteractionData(test: unknown): test is TInteractionData {
	if (!isTData(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, 'interaction')) return false;

	return true;
}

export function encodeIData<
		I extends IData|IInteractionData|IPersonData = IData,
		T extends TData|TInteractionData|TPersonData = TData
>(data: I): T {
	const base: TData = {
			event: data.event,
			uid: data.uid,
			
			id: data.id,
			logno: data.logno,
			timestamp: commonsDateDateToYmdHis(data.timestamp, true),

			field: data.field,

			// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
			value: (data.value === undefined ? null : (commonsTypeIsDate(data.value) ? commonsDateDateToYmdHis(data.value, true) : data.value)) as any	// aparent bug in tsc's own linting
	};
	
	if (isIPersonData(data)) (base as TPersonData).person = data.person;
	if (isIInteractionData(data)) (base as TInteractionData).interaction = data.interaction;
	
	return base as T;
}

export function decodeIData<
		I extends IData|IInteractionData|IPersonData = IData,
		T extends TData|TInteractionData|TPersonData = TData
>(encoded: T): I {
	const base: IData = {
			event: encoded.event,
			uid: encoded.uid,
			
			id: encoded.id,
			logno: encoded.logno,
			timestamp: commonsDateYmdHisToDate(encoded.timestamp, true),

			field: encoded.field,
			
			value: encoded.value === null ? undefined : encoded.value
	};

	// a bit dirty; technically strings typed as YmhHis could be wrongly parsed, but it's not likely.
	// can fix in the future if desired
	if (commonsTypeIsString(base.value) && COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(base.value)) {
		base.value = commonsDateYmdHisToDate(base.value, true);
	}
	
	if (isTPersonData(encoded)) (base as IPersonData).person = encoded.person;
	if (isTInteractionData(encoded)) (base as IInteractionData).interaction = encoded.interaction;
	
	return base as I;
}
