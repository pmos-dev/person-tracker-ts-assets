import {
		commonsTypeHasProperty,
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyBoolean,
		TEncodedObject
} from 'tscommons-es-core';
import { TCommonsStepFlowCondition, isTCommonsStepFlowCondition, encodeICommonsStepFlowCondition, decodeICommonsStepFlowCondition } from 'tscommons-es-models-step-flow';

import { ICondition } from '../interfaces/icondition';

export type TCondition = TCommonsStepFlowCondition & {
		value: number|string|boolean|null;
} & TEncodedObject;

export function isTCondition(test: unknown): test is TCondition {
	if (!isTCommonsStepFlowCondition(test)) return false;
	
	if (!commonsTypeHasProperty(test, 'value')) return false;
	
	if (
		test['value'] !== null
		&& !commonsTypeHasPropertyNumber(test, 'value')
		&& !commonsTypeHasPropertyString(test, 'value')
		&& !commonsTypeHasPropertyBoolean(test, 'value')
	) return false;
	
	return true;
}

export function encodeICondition(condition: ICondition): TCondition {
	return {
			// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
			value: (condition.value === undefined ? null : condition.value) as any,	// aparent bug in tsc's own linting
			...encodeICommonsStepFlowCondition(condition)
	};
}

export function decodeICondition(encoded: TCondition): ICondition {
	return {
			value: encoded.value === null ? undefined : encoded.value,
			...decodeICommonsStepFlowCondition(encoded)
	};
}
