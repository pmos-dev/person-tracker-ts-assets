import { commonsTypeEncodePropertyObject, TEncodedObject } from 'tscommons-es-core';

import { IInteraction, isIInteraction } from '../interfaces/iinteraction';

export type TInteraction = Readonly<IInteraction> & TEncodedObject;

export function isTInteraction(test: unknown): test is TInteraction {
	if (!isIInteraction(test)) return false;
	
	return true;
}

export function encodeIInteraction(interaction: IInteraction): TInteraction {
	return commonsTypeEncodePropertyObject({ ...interaction }) as TInteraction;
}

export function decodeIInteraction(encoded: TInteraction): IInteraction {
	return { ...encoded };
}
