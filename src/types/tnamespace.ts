import { commonsTypeEncodePropertyObject, TEncodedObject } from 'tscommons-es-core';
import { INamespace, isINamespace } from '../interfaces/inamespace';

export type TNamespace = Readonly<INamespace> & TEncodedObject;

export function isTNamespace(test: unknown): test is TNamespace {
	if (!isINamespace(test)) return false;
	
	return true;
}

export function encodeINamespace(namespace: INamespace): TNamespace {
	return commonsTypeEncodePropertyObject({ ...namespace }) as TNamespace;
}

export function decodeINamespace(encoded: TNamespace): INamespace {
	return { ...encoded };
}
