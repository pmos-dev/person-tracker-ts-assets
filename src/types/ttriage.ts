import { commonsTypeEncodePropertyObject, TEncodedObject } from 'tscommons-es-core';

import { ITriage, isITriage } from '../interfaces/itriage';

export type TTriage = Readonly<ITriage> & TEncodedObject;

export function isTTriage(test: unknown): test is TTriage {
	if (!isITriage(test)) return false;
	
	return true;
}

export function encodeITriage(triage: ITriage): TTriage {
	return commonsTypeEncodePropertyObject({ ...triage }) as TTriage;
}

export function decodeITriage(encoded: TTriage): ITriage {
	return { ...encoded };
}
