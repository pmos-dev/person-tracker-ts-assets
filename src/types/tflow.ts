import { commonsTypeEncodePropertyObject, TEncodedObject } from 'tscommons-es-core/dist';
import { TCommonsStepFlowFlow, isTCommonsStepFlowFlow, encodeICommonsStepFlowFlow, decodeICommonsStepFlowFlow } from 'tscommons-es-models-step-flow';

import { IFlow } from '../interfaces/iflow';

export type TFlow = TCommonsStepFlowFlow & TEncodedObject;

export function isTFlow(test: unknown): test is TFlow {
	if (!isTCommonsStepFlowFlow(test)) return false;
	
	return true;
}

export function encodeIFlow(flow: IFlow): TFlow {
	return commonsTypeEncodePropertyObject({ ...encodeICommonsStepFlowFlow(flow) }) as TFlow;
}

export function decodeIFlow(encoded: TFlow): IFlow {
	return { ...decodeICommonsStepFlowFlow(encoded) };
}
