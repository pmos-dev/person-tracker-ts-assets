import { ECommonsStepFlowStepType } from 'tscommons-es-models-step-flow';

export type TStepFlowRenderedStep = {
		type: ECommonsStepFlowStepType;
		uid: string;
		flows: TStepFlowRenderedFlow[];
		field: string|null;
};

export type TStepFlowRenderedFlow = {
		value: number|string|boolean|null;
		outstep: string;
};

export type TStepFlowRenderedTriage = {
		root: string;
		steps: TStepFlowRenderedStep[];
};
