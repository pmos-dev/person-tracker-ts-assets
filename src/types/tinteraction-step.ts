import { commonsDateDateToYmdHis, commonsDateYmdHisToDate, commonsTypeHasPropertyNumber, commonsTypeHasPropertyNumberOrUndefined, commonsTypeHasPropertyString, COMMONS_REGEX_PATTERN_DATETIME_YMDHIS } from 'tscommons-es-core';
import { ICommonsSecondClass, isICommonsSecondClass } from 'tscommons-es-models';

import { IInteraction } from '../interfaces/iinteraction';
import { IInteractionStep } from '../interfaces/iinteraction-step';

export type TInteractionStep = ICommonsSecondClass<IInteraction> & {
		interaction: number;
		step: number;
		timestamp: string;
};

// This isn't be used at the moment. Future possibilities
type TInteractionStepWithData = TInteractionStep & {
		data: number;
};

export function isTInteractionStep(test: unknown): test is TInteractionStep {
	if (!isICommonsSecondClass<IInteraction>(test, 'interaction')) return false;

	if (!commonsTypeHasPropertyNumber(test, 'step')) return false;
	if (!commonsTypeHasPropertyString(test, 'timestamp')) return false;
	if (!COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(test['timestamp'] as string)) return false;

	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'data')) return false;

	return true;
}

export function encodeIInteractionStep(interactionStep: IInteractionStep): TInteractionStep {
	const base: TInteractionStep = {
			interaction: interactionStep.interaction,

			id: interactionStep.id,
			step: interactionStep.step,
			timestamp: commonsDateDateToYmdHis(interactionStep.timestamp, true)
	};

	if (interactionStep.data) (base as TInteractionStepWithData).data = interactionStep.data;

	return base;
}

export function decodeIInteractionStep(encoded: TInteractionStep): IInteractionStep {
	const decoded: IInteractionStep = {
			interaction: encoded.interaction,

			id: encoded.id,
			step: encoded.step,
			timestamp: commonsDateYmdHisToDate(encoded.timestamp, true)
	};

	if ((encoded as TInteractionStepWithData).data) decoded.data = (encoded as TInteractionStepWithData).data;

	return decoded;
}
