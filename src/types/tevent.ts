import {
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyString,
		commonsDateDateToYmdHis,
		commonsDateYmdHisToDate,
		COMMONS_REGEX_PATTERN_DATETIME_YMDHIS,
		TEncodedObject
} from 'tscommons-es-core';
import { ICommonsSecondClass, ICommonsUniquelyIdentifiedBase62, isICommonsUniquelyIdentifiedBase62 } from 'tscommons-es-models';
import { ICommonsManaged } from 'tscommons-es-models-adamantine';

import { IEvent } from '../interfaces/ievent';
import { TNamespace } from './tnamespace';

export type TEvent = ICommonsManaged & ICommonsSecondClass<TNamespace> & ICommonsUniquelyIdentifiedBase62 & {
		namespace: number;
		description: string;
		start: string;
		end: string;
} & TEncodedObject;

export function isTEvent(test: unknown): test is TEvent {
	if (!commonsTypeHasPropertyNumber(test, 'namespace')) return false;
	if (!isICommonsUniquelyIdentifiedBase62(test)) return false;
	
	if (!commonsTypeHasPropertyNumber(test, 'id')) return false;
	if (!commonsTypeHasPropertyString(test, 'name')) return false;
	if (!commonsTypeHasPropertyString(test, 'start')) return false;
	if (!COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(test['start'] as string)) return false;
	if (!commonsTypeHasPropertyString(test, 'end')) return false;
	if (!COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(test['end'] as string)) return false;
	if (!commonsTypeHasPropertyString(test, 'description')) return false;
	
	return true;
}

export function encodeIEvent(event: IEvent): TEvent {
	return {
			namespace: event.namespace,
			
			id: event.id,
			uid: event.uid,
			name: event.name,
			start: commonsDateDateToYmdHis(event.start, true),
			end: commonsDateDateToYmdHis(event.end, true),
			description: event.description
	};
}

export function decodeIEvent(encoded: TEvent): IEvent {
	return {
			namespace: encoded.namespace,
			
			id: encoded.id,
			uid: encoded.uid,
			name: encoded.name,
			start: commonsDateYmdHisToDate(encoded.start, true),
			end: commonsDateYmdHisToDate(encoded.end, true),
			description: encoded.description
	};
}
