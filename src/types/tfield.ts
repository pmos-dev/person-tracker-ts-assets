import { commonsTypeHasPropertyNumber, commonsTypeHasPropertyString, TEncodedObject } from 'tscommons-es-core';
import { ICommonsOrientatedOrdered, ICommonsUniquelyIdentifiedBase62, isICommonsUniquelyIdentifiedBase62 } from 'tscommons-es-models';
import { isTCommonsAdamantineField, encodeICommonsAdamantineField, decodeICommonsAdamantineField, TCommonsAdamantineField } from 'tscommons-es-models-field';
import { ICommonsManaged } from 'tscommons-es-models-adamantine';

import { IField } from '../interfaces/ifield';

import { isEFieldContext, EFieldContext } from '../enums/efield-context';
import { EDataMethod, isEDataMethod } from '../enums/edata-method';

import { TNamespace } from './tnamespace';

export type TField = ICommonsManaged & ICommonsOrientatedOrdered<TNamespace> & TCommonsAdamantineField & ICommonsUniquelyIdentifiedBase62 & {
		namespace: number;
		context: EFieldContext;
		method: EDataMethod;
} & TEncodedObject;

export function isTField(test: unknown): test is TField {
	if (!commonsTypeHasPropertyNumber(test, 'namespace')) return false;
	if (!isICommonsUniquelyIdentifiedBase62(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, 'id')) return false;
	if (!commonsTypeHasPropertyString(test, 'name')) return false;

	if (!commonsTypeHasPropertyString(test, 'context')) return false;
	if (!isEFieldContext(test['context'])) return false;

	if (!commonsTypeHasPropertyString(test, 'method')) return false;
	if (!isEDataMethod(test['method'])) return false;

	if (!isTCommonsAdamantineField(test)) return false;
	
	return true;
}

export function encodeIField(field: IField): TField {
	return {
			namespace: field.namespace,
			id: field.id,
			uid: field.uid,
			ordered: field.ordered,
			context: field.context,
			method: field.method,
			...encodeICommonsAdamantineField(field)
	};
}

export function decodeIField(encoded: TField): IField {
	return {
			namespace: encoded.namespace,
			id: encoded.id,
			uid: encoded.uid,
			ordered: encoded.ordered,
			context: encoded.context,
			method: encoded.method,
			// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
			...decodeICommonsAdamantineField(encoded as any)	// aparent bug in tsc's own linting
	};
}
