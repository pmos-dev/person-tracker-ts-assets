import {
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyNumberOrUndefined
} from 'tscommons-es-core';
import { isTCommonsStepFlowStep, encodeICommonsStepFlowStep, decodeICommonsStepFlowStep, TCommonsStepFlowStep } from 'tscommons-es-models-step-flow';
import { ICommonsSecondClass, isICommonsUniquelyIdentifiedBase62 } from 'tscommons-es-models';

import { IStep } from '../interfaces/istep';

import { TTriage } from './ttriage';

export type TStep = ICommonsSecondClass<TTriage> & TCommonsStepFlowStep & {
		triage: number;
};

type TStepWithField = TStep & { field: number };

export function isTStep(test: unknown): test is TStep {
	if (!commonsTypeHasPropertyNumber(test, 'triage')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'field')) return false;
	if (!isICommonsUniquelyIdentifiedBase62(test)) return false;
	
	if (!isTCommonsStepFlowStep(test)) return false;
	
	return true;
}

export function encodeIStep(step: IStep): TStep {
	const encoded: TStep = {
			triage: step.triage,
			...encodeICommonsStepFlowStep(step)
	};
	
	if (step.field !== undefined) (encoded as TStepWithField).field = step.field;

	return encoded;
}

export function decodeIStep(encoded: TStep): IStep {
	const decoded: IStep = {
			triage: encoded.triage,
			...decodeICommonsStepFlowStep(encoded)
	};
	
	if ((encoded as TStepWithField).field !== undefined) decoded.field = (encoded as TStepWithField).field;
	
	return decoded;
}
