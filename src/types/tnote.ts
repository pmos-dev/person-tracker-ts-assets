import {
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyNumberOrUndefined,
		commonsDateDateToYmdHis,
		commonsDateYmdHisToDate,
		COMMONS_REGEX_PATTERN_DATETIME_YMDHIS,
		TEncodedObject
} from 'tscommons-es-core';
import { ICommonsSecondClass, ICommonsUniquelyIdentifiedBase62, isICommonsUniquelyIdentifiedBase62 } from 'tscommons-es-models';

import { INote, IInteractionNote, IPersonNote, isIInteractionNote, isIPersonNote } from '../interfaces/inote';

import { TEvent } from './tevent';

export type TNote = ICommonsSecondClass<TEvent> & ICommonsUniquelyIdentifiedBase62 & {
		event: number;
		logno: number;
		timestamp: string;
		value: string;
} & TEncodedObject;

export type TPersonNote = TNote & { person: number };
export type TInteractionNote = TNote & { interaction: number };

export function isTNote(test: unknown): test is TNote {
	if (!commonsTypeHasPropertyNumber(test, 'event')) return false;

	if (!isICommonsUniquelyIdentifiedBase62(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, 'id')) return false;

	if (!commonsTypeHasPropertyNumber(test, 'logno')) return false;

	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'person')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'interaction')) return false;

	if (test['person'] === undefined && test['interaction'] === undefined) return false;
	if (test['person'] !== undefined && test['interaction'] !== undefined) return false;
	
	if (!commonsTypeHasPropertyString(test, 'timestamp')) return false;
	if (!COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(test['timestamp'] as string)) return false;

	if (!commonsTypeHasPropertyString(test, 'value')) return false;
	
	return true;
}

export function isTPersonNote(test: unknown): test is TPersonNote {
	if (!isTNote(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, 'person')) return false;

	return true;
}

export function isTInteractionNote(test: unknown): test is TInteractionNote {
	if (!isTNote(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, 'interaction')) return false;

	return true;
}

export function encodeINote<
		I extends INote|IInteractionNote|IPersonNote = INote,
		T extends TNote|TInteractionNote|TPersonNote = TNote
>(note: I): T {
	const base: TNote = {
			event: note.event,
			uid: note.uid,
			
			id: note.id,
			logno: note.logno,
			timestamp: commonsDateDateToYmdHis(note.timestamp, true),
			value: note.value
	};
	
	if (isIPersonNote(note)) (base as TPersonNote).person = note.person;
	if (isIInteractionNote(note)) (base as TInteractionNote).interaction = note.interaction;
	
	return base as T;
}

export function decodeINote<
		I extends INote|IInteractionNote|IPersonNote = INote,
		T extends TNote|TInteractionNote|TPersonNote = TNote
>(encoded: T): I {
	const base: INote = {
			event: encoded.event,
			uid: encoded.uid,
			
			id: encoded.id,
			logno: encoded.logno,
			timestamp: commonsDateYmdHisToDate(encoded.timestamp, true),

			value: encoded.value
	};
	
	if (isTPersonNote(encoded)) (base as IPersonNote).person = encoded.person;
	if (isTInteractionNote(encoded)) (base as IInteractionNote).interaction = encoded.interaction;
	
	return base as I;
}
