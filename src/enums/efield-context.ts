import { commonsTypeIsString } from 'tscommons-es-core';

export enum EFieldContext {
		PERSON = 'person',
		INTERACTION = 'interaction'
}

export function toEFieldContext(type: string): EFieldContext|undefined {
	switch (type) {
		case EFieldContext.PERSON.toString():
			return EFieldContext.PERSON;
		case EFieldContext.INTERACTION.toString():
			return EFieldContext.INTERACTION;
	}
	return undefined;
}

export function fromEFieldContext(type: EFieldContext): string {
	switch (type) {
		case EFieldContext.PERSON:
			return EFieldContext.PERSON.toString();
		case EFieldContext.INTERACTION:
			return EFieldContext.INTERACTION.toString();
	}
	
	throw new Error('Unknown EFieldContext');
}

export function isEFieldContext(test: unknown): test is EFieldContext {
	if (!commonsTypeIsString(test)) return false;
	
	return toEFieldContext(test) !== undefined;
}

export function keyToEFieldContext(key: string): EFieldContext {
	switch (key) {
		case 'PERSON':
			return EFieldContext.PERSON;
		case 'INTERACTION':
			return EFieldContext.INTERACTION;
	}
	
	throw new Error(`Unable to obtain EFieldContext for key: ${key}`);
}

export const EFIELD_CONTEXTS: EFieldContext[] = Object.keys(EFieldContext)
		.map((e: string): EFieldContext => keyToEFieldContext(e));
