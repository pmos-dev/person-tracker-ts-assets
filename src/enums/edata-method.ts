import { commonsTypeIsString } from 'tscommons-es-core';

export enum EDataMethod {
		LATEST = 'latest',
		FIRST = 'first',
		SEQUENCE = 'sequence',
		EARLIEST = 'earliest'	// earliest datetime, so only makes sense for Date
}

export function toEDataMethod(type: string): EDataMethod|undefined {
	switch (type) {
		case EDataMethod.LATEST.toString():
			return EDataMethod.LATEST;
		case EDataMethod.FIRST.toString():
			return EDataMethod.FIRST;
		case EDataMethod.SEQUENCE.toString():
			return EDataMethod.SEQUENCE;
		case EDataMethod.EARLIEST.toString():
			return EDataMethod.EARLIEST;
	}
	return undefined;
}

export function fromEDataMethod(type: EDataMethod): string {
	switch (type) {
		case EDataMethod.LATEST:
			return EDataMethod.LATEST.toString();
		case EDataMethod.FIRST:
			return EDataMethod.FIRST.toString();
		case EDataMethod.SEQUENCE:
			return EDataMethod.SEQUENCE.toString();
		case EDataMethod.EARLIEST:
			return EDataMethod.EARLIEST.toString();
	}
	
	throw new Error('Unknown EDataMethod');
}

export function isEDataMethod(test: unknown): test is EDataMethod {
	if (!commonsTypeIsString(test)) return false;
	
	return toEDataMethod(test) !== undefined;
}

export function keyToEDataMethod(key: string): EDataMethod {
	switch (key) {
		case 'LATEST':
			return EDataMethod.LATEST;
		case 'FIRST':
			return EDataMethod.FIRST;
		case 'SEQUENCE':
			return EDataMethod.SEQUENCE;
		case 'EARLIEST':
			return EDataMethod.EARLIEST;
	}
	
	throw new Error(`Unable to obtain EDataMethod for key: ${key}`);
}

export const EDATA_METHODS: EDataMethod[] = Object.keys(EDataMethod)
		.map((e: string): EDataMethod => keyToEDataMethod(e));
